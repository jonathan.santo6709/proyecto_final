<?php
      class Dosis extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("dosi");
            $this->load->model("vacuna");
            $this->load->model("persona");
            //validando si alguien esta conectado
            if ($this->session->userdata("c0nectadoUTC")) {
              // si esta conectado
            } else {
              redirect("seguridades/formularioLogin");
            }
        }

        public function index(){
          $data["listadoDosis"]=$this->dosi->consultarTodos();
          $this->load->view("header");
          $this->load->view("dosis/index",$data);
          $this->load->view("footer");
        }
        public function nuevo(){
          $data["listadoVacunas"]=$this->vacuna->obtenerTodos();
          $data["listadoPersonas"]=$this->persona->consultarTodos();
          $this->load->view("header");
          $this->load->view("dosis/nuevo",$data);
          $this->load->view("footer");
        }

        public function editar($id_ds){
          $data["listadoVacunas"]=$this->vacuna->obtenerTodos();
          $data ['listadoPersonas']=$this->persona->consultarTodos();
          $data["dosi"]=$this->dosi->consultarPorId($id_ds);
          $this->load->view("header");
          $this->load->view("dosis/editar",$data);
          $this->load->view("footer");
        }

        public function procesarActualizacion(){
            $id_ds=$this->input->post("id_ds");
            $datosDosiEditado=array(
                "numero_ds"=>$this->input->post("numero_ds"),
                "lugar_ds"=>$this->input->post("lugar_ds"),
                "fecha_dosis_ds"=>$this->input->post("fecha_dosis_ds"),
                "fk_id_vac"=>$this->input->post("fk_id_vac"),
                "fk_id_per"=>$this->input->post("fk_id_per")
            );

          if ($this->dosi->actualizar($id_ds,$datosDosiEditado)) {
            //echo "INSERCION EXITOSA";
            redirect("dosis/index");
          } else {
            echo "ERROR AL ACTUALIZAR";
          }
        }

        public function guardarDosi(){
            $datosNuevoDosi=array(
              "numero_ds"=>$this->input->post("numero_ds"),
              "lugar_ds"=>$this->input->post("lugar_ds"),
              "fecha_dosis_ds"=>$this->input->post("fecha_dosis_ds"),
              "fk_id_vac"=>$this->input->post("fk_id_vac"),
              "fk_id_per"=>$this->input->post("fk_id_per")
              );

          if ($this->dosi->insertar($datosNuevoDosi)) {
              $this->session->set_flashdata("confirmacion","CLIENTE INSERTADO EXITOSAMENTE.");
          } else {
            $this->session->set_flashdata("error","ERROR AL PROCESAR, INTENTE NUEVAMENTE.");
          }
          redirect("dosis/index");

        }
        function procesarEliminacion($id_ds){
                if ($this->session->userdata("c0nectadoUTC")->perfil_usu=="ADMINISTRADOR") {
                  if ($this->dosi->eliminar($id_ds)) {
                    $this->session->set_flashdata("confirmacion","Dosis eliminado exitosamente.");
                  } else {
                    $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
                  }
                  redirect("dosis/index");
                } else {
                  redirect("seguridades/formularioLogin");
                }
          }
        }//cierre funcion
     ?>
