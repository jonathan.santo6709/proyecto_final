<?php
class Vacunas extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model("vacuna");
        $this->load->model("dosi");
        //validando si alguien esta conectado ESTO ES LO PRIMORDIAL
        if ($this->session->userdata("c0nectadoUTC")) {
          // si esta conectado
          if ($this->session->userdata("c0nectadoUTC")->perfil_usu=="ADMINISTRADOR")
          {
            // SI ES ADMINISTRADOR
          } else {
            redirect("/");
          }
        } else {
          redirect("seguridades/formularioLogin");
        }
    }


  public function index(){
    $this->load->view("header");
    $this->load->view("vacunas/index");
    $this->load->view("footer");
  }

  //funcion para frenderizar la vista listado.php
  public function listado(){
    $data["listadoDosis"]=$this->dosi->consultarTodos();
    $data["listadoVacunas"]=$this->vacuna->obtenerTodos();
    $this->load->view("vacunas/listado",$data);

  }
  ///Inserciion Asincrona
  public function insertarVacuna(){
    $data["listadoDosis"]=$this->dosi->consultarTodos();
    $data=array(
    "nombre_vac"=>$this->input->post("nombre_vac"),
    "descripcion_vac"=>$this->input->post("descripcion_vac"),
    "costo_vac"=>$this->input->post("costo_vac"),
    "miligramos_vac"=>$this->input->post("miligramos_vac"),
    "foto_vac"=>$this->input->post("foto_vac"),
    "fk_id_ds"=>$this->input->post("fk_id_ds")
    );

    //logica de negocio necesarioa para subir la fotografia del vacuna

    $this->load->library('upload');//carga de la libreria de subida de archivos
    $nombreTemporal='foto_vacuna_'.time().'_'.rand(1,500);//generar nombre aleatorio
    $config['file_name']=$nombreTemporal; //te,poral
    $config['upload_path']=APPPATH.'../uploads/vacunas/'; //ruta de la subida de los archivos
    $config['allowed_types']='jpeg|jpg|png';//tipo de datos permitidos
    $config['max_size']=4*1024;//configurar el peso maximo de los archivos
    $this->upload->initialize($config);//inicializar la configuracion
    //instruccion para que se valide la subida del archivos
    if($this->upload->do_upload('foto_vac')){
    //FUNCION PARA QUE SE ME ACTUALICE LA FOTO CUANDO PONGO UNA NUEVA FOTO
    $query =$this->vacuna->obtenerPorId($id_vac);
    unlink(APPPATH.'../uploads/vacunas/'.$query->foto_vac);


    $dataSubida=$this->upload->data();
    $datosVacunaEditado['foto_vac']=$dataSubida['file_name'];

    }

    if($this->vacuna->insertarVacuna($data)){
      echo json_encode(array("respuesta"=>"ok"));
    }else{
      echo json_encode(array("respuesta"=>"error"));

    }
  }
  public function eliminarVacuna(){
    $id_vac=$this->input->post("id_vac");
    if ($this->vacuna->eliminar($id_vac)) {
      echo json_encode(array("respuesta"=>"ok"));
    } else{
      echo json_encode(array("respuesta"=>"error"));
    }
  }

  public function editar($id_vac){
      $data["vacuna"]=$this->vacuna->obtenerPorId($id_vac);
      $this->load->view("vacunas/editar",$data);
      }

      public function actualizarVacunaAjax(){
              $id_vac=$this->input->post("id_vac");
              $data=array(
                "nombre_vac"=>$this->input->post("nombre_vac"),
                "descripcion_vac"=>$this->input->post("descripcion_vac"),
                "costo_vac"=>$this->input->post("costo_vac"),
                "miligramos_vac"=>$this->input->post("miligramos_vac"),
                "foto_vac"=>$this->input->post("foto_vac"),
                "fk_id_ds"=>$this->input->post("fk_id_ds")
              );

              //logica de negocio necesarioa para subir la fotografia del vacuna

              $this->load->library('upload');//carga de la libreria de subida de archivos
              $nombreTemporal='foto_vacuna_'.time().'_'.rand(1,500);//generar nombre aleatorio
              $config['file_name']=$nombreTemporal; //te,poral
              $config['upload_path']=APPPATH.'../uploads/vacunas/'; //ruta de la subida de los archivos
              $config['allowed_types']='jpeg|jpg|png';//tipo de datos permitidos
              $config['max_size']=4*1024;//configurar el peso maximo de los archivos
              $this->upload->initialize($config);//inicializar la configuracion
              //instruccion para que se valide la subida del archivos
              if($this->upload->do_upload('foto_vac')){
                //FUNCION PARA QUE SE ME ACTUALICE LA FOTO CUANDO PONGO UNA NUEVA FOTO
              $query =$this->vacuna->obtenerPorId($id_vac);
              unlink(APPPATH.'../uploads/vacunas/'.$query->foto_vac);


              $dataSubida=$this->upload->data();
              $datosVacunaEditado['foto_vac']=$dataSubida['file_name'];

              }

              if($this->vacuna->actualizar($data,$id_vac)){
                  echo json_encode(array("respuesta"=>"ok"));
              }else{
                  echo json_encode(array("respuesta"=>"error"));
              }
          }



  }//cierre de la clase
