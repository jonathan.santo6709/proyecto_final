<?php
      class Personas extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("persona");
            $this->load->model("genero");
            //validando si alguien esta conectado
            if ($this->session->userdata("c0nectadoUTC")) {
              // si esta conectado
            } else {
              redirect("seguridades/formularioLogin");
            }
        }

        public function index(){
          $data["listadoPersonas"]=$this->persona->consultarTodos();
          $this->load->view("header");
          $this->load->view("personas/index",$data);
          $this->load->view("footer");
        }
        public function nuevo(){
          $data["listadoGeneros"]=$this->genero->consultarTodos();
          $this->load->view("header");
          $this->load->view("personas/nuevo",$data);
          $this->load->view("footer");
        }

        public function editar($id_per){
          $data ['listadoGeneros']=$this->genero->consultarTodos();
          $data["persona"]=$this->persona->consultarPorId($id_per);
          $this->load->view("header");
          $this->load->view("personas/editar",$data);
          $this->load->view("footer");
        }

        public function procesarActualizacion(){
            $id_per=$this->input->post("id_per");
            $datosPersonaEditado=array(
                "identificacion_per"=>$this->input->post("identificacion_per"),
                "apellido_per"=>$this->input->post("apellido_per"),
                "nombre_per"=>$this->input->post("nombre_per"),
                "telefono_per"=>$this->input->post("telefono_per"),
                "direccion_per"=>$this->input->post("direccion_per"),
                "email_per"=>$this->input->post("email_per"),
                "estado_per"=>$this->input->post("estado_per"),
                "fk_id_gen"=>$this->input->post("fk_id_gen")

            );
            $this->load->library("upload");
          $new_name = "foto_persona_" . time() . "_" . rand(1, 5000);
          $config["file_name"]=$nombreTemporal;
          $config["upload_path"]=APPPATH.'../uploads/personas/';
          $config['allowed_types']        = 'jpeg|jpg|png';
          $config['max_size']             = 4*1024;
          $this->upload->initialize($config);

          if ($this->upload->do_upload("foto_per")) {
            $dataSubida = $this->upload->data();
            $datosPersonaEditado["foto_per"] = $dataSubida['file_name'];
          }
          $this->session->set_flashdata('confirmacion','ACTUALIZADO EXITOSAMENTE');


          if ($this->persona->actualizar($id_per,$datosPersonaEditado)) {
            //echo "INSERCION EXITOSA";
            redirect("personas/index");
          } else {
            echo "ERROR AL ACTUALIZAR";
          }
        }

        public function guardarPersona(){
            $datosNuevoPersona=array(
                "identificacion_per"=>$this->input->post("identificacion_per"),
                "apellido_per"=>$this->input->post("apellido_per"),
                "nombre_per"=>$this->input->post("nombre_per"),
                "telefono_per"=>$this->input->post("telefono_per"),
                "direccion_per"=>$this->input->post("direccion_per"),
                "email_per"=>$this->input->post("email_per"),
                "estado_per"=>$this->input->post("estado_per"),
                "fk_id_gen"=>$this->input->post("fk_id_gen")

              );
          //logiga de negocio para subir la foto del cliente
          $this->load->library("upload");//carga de la libreria de subida de archivos
          $nombreTemporal="foto_persona_".time()."_".rand(1,5000);
          $config["file_name"]=$nombreTemporal;
          $config["upload_path"]=APPPATH.'../uploads/personas/';
          $config["allowed_types"]="jpeg|jpg|png";
          $config["max_size"]=2*1024;//2mb
          $this->upload->initialize($config);
          //codigo para subir el archivo y guradar el nombre en la BBD
          if ($this->upload->do_upload("foto_per")) {
            $dataSubida=$this->upload->data();
            $datosNuevoPersona["foto_per"]=$dataSubida["file_name"];
          }

          if ($this->persona->insertar($datosNuevoPersona)) {
              $this->session->set_flashdata("confirmacion","CLIENTE INSERTADO EXITOSAMENTE.");
          } else {
            $this->session->set_flashdata("error","ERROR AL PROCESAR, INTENTE NUEVAMENTE.");
          }
          redirect("personas/index");

        }
        function procesarEliminacion($id_per){
                if ($this->session->userdata("c0nectadoUTC")->perfil_usu=="ADMINISTRADOR") {
                  if ($this->persona->eliminar($id_per)) {
                    $this->session->set_flashdata("confirmacion","Cliente eliminado exitosamente.");
                  } else {
                    $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
                  }
                  redirect("personas/index");
                } else {
                  redirect("seguridades/formularioLogin");
                }
          }
        }//cierre funcion
     ?>
