<?php
    class Dosi extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db -> insert("dosi",$datos);
      //funcion para actualizar
    }
      public function actualizar($id_ds,$datos){
        $this->db->where("id_ds",$id_ds);
        return $this->db->update("dosi",$datos);
      }
      //funcion para sacar el detalle de un dosis
      public function consultarPorId($id_ds){
        $this->db->where("id_ds",$id_ds);
        $this->db->join('vacuna','vacuna.id_vac=dosi.fk_id_vac');
        $this->db->join('persona','persona.id_per=dosi.fk_id_per');
        $dosi=$this->db->get("dosi");
        if($dosi->num_rows()>0){
          return $dosi->row();//cuando SI hay dosis
        }else{
          return false;//cuando NO hay dosis
        }
      }
      //funcion para consultar todos lo dosis
      public function consultarTodos(){
        $this->db->join('vacuna','vacuna.id_vac=dosi.fk_id_vac');
        $this->db->join('persona','persona.id_per=dosi.fk_id_per');
          $listadoDosis=$this->db->get("dosi");
          if($listadoDosis->num_rows()>0){
            return $listadoDosis;//cuando SI hay personas
          }else{
            return false;//cuando NO hay personas
          }
      }

      public function eliminar($id_ds){
        $this->db->where("id_ds",$id_ds);
        return $this->db->delete("dosi");
      }


      public function obtenerPorId($id_ds){
   $this->db->where('id_ds',$id_ds);
   $query= $this->db->get('dosi');
   if ($query->num_rows()>0) {
     // code...
     return $query->row();
   } else {
     return false;
   }
 }
/*
 public function obtenerDosisPorEstado($estado){
  $this->db->where("estado_per",$estado);
  $dosis=$this->db->get("dosi");
  if($dosis->num_rows()>0){
    return $dosis;
  }else{
    return false;
  }
}*/

   }//cierre de la clase



   //
 ?>
