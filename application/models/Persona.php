<?php
    class Persona extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db -> insert("persona",$datos);
      //funcion para actualizar
    }
      public function actualizar($id_per,$datos){
        $this->db->where("id_per",$id_per);
        return $this->db->update("persona",$datos);
      }
      //funcion para sacar el detalle de un cliente
      public function consultarPorId($id_per){
        $this->db->where("id_per",$id_per);
        $this->db->join('genero','genero.id_gen=persona.fk_id_gen');
        $persona=$this->db->get("persona");
        if($persona->num_rows()>0){
          return $persona->row();//cuando SI hay personas
        }else{
          return false;//cuando NO hay personas
        }
      }
      //funcion para consultar todos lo personas
      public function consultarTodos(){
        $this->db->join('genero','genero.id_gen=persona.fk_id_gen');
          $listadoPersonas=$this->db->get("persona");
          if($listadoPersonas->num_rows()>0){
            return $listadoPersonas;//cuando SI hay personas
          }else{
            return false;//cuando NO hay personas
          }
      }

      public function eliminar($id_per){
        $this->db->where("id_per",$id_per);
        return $this->db->delete("persona");
      }


      public function obtenerPorId($id_per){
   $this->db->where('id_per',$id_per);
   $query= $this->db->get('persona');
   if ($query->num_rows()>0) {
     // code...
     return $query->row();
   } else {
     return false;
   }
 }

 public function obtenerPersonasPorEstado($estado){
  $this->db->where("estado_per",$estado);
  $personas=$this->db->get("persona");
  if($personas->num_rows()>0){
    return $personas;
  }else{
    return false;
  }
}

   }//cierre de la clase



   //
 ?>
