<?php
class Vacuna extends CI_Model {
  public function __construct(){
    parent::__construct();
  }

public function insertarVacuna($data){
  return $this->db->insert('vacuna',$data);
}

public function obtenerTodos(){
  $this->db->join('dosi','dosi.id_ds=vacuna.fk_id_ds');
$listado=$this->db->get("vacuna");
if ($listado->num_rows()>0) {
  return $listado;
} else {
  return false;

    }
  }


  public function eliminar($id_vac){
    $this->db->where("id_vac",$id_vac);
  return  $this->db->delete("vacuna");
  }


  public function obtenerPorId($id_vac){
    $this->db->join('dosi','dosi.id_ds=vacuna.fk_id_ds');
                  $this->db->where("id_vac",$id_vac);
                  $vacuna=$this->db->get("vacuna");
                  if($vacuna->num_rows()>0){
                    return $vacuna->row();
                  }else{//cuando las credenciales estan incorrectas
                    return false;
                  }
                    }

  public function actualizar($data, $id_vac){
        $this->db->where("id_vac",$id_vac);
        return $this->db->update("vacuna",$data);
    }


}//cierre de la clase usuario

?>
