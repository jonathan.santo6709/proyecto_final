<br>
<form  action="<?php echo site_url(); ?>/personas/procesarActualizacion" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_per"  id="id_per" value="<?php echo $persona->id_per; ?>">
<br>
<br>
<!-- <div class="row"> -->
<table class="table table-success table-striped">

    <div class="col-md-10" >
  <div class="row">
    <div class="col-md-4" ></div>
        <div class="col-md-8">
          <center>
            <b><label for="">GENERO</label></b>
            <select class="form-control" name="fk_id_gen" id="fk_id_gen" required >
              <option value="">SELECCIONE UN GENERO PORFAVOR</option>
              <?php if ($listadoGeneros): ?>
              <?php foreach ($listadoGeneros->result() as $generoTemporal): ?>
                <option value="<?php echo $generoTemporal->id_gen; ?>">
                  <?php echo $generoTemporal->nombre_gen; ?>
                </option>
              <?php endforeach; ?>
            <?php endif; ?>

            </select>
            <br>
  <label for"">IDENTIFICACION:</label>
    <br>
    <input type="text" class="form-control" value="<?php echo $persona->identificacion_per; ?>" type="text" name="identificacion_per" id="identificacion_per" value="" placeholder="Por favor ingrese la identificacion">
    <br>
<label for"">NOMBRE:</label>
    <br>
    <input type="text" class="form-control" value="<?php echo $persona->nombre_per; ?>" type="text" name="nombre_per" id= "nombre_per"value="" placeholder="Ingrese su nombre">
    <br>
  <label for"">APELLIDO:</label>
    <br>
    <input type="text" class="form-control" value="<?php echo $persona->apellido_per; ?>" type="text" name="apellido_per" id= "apellido_per"value="" placeholder="Ingrese el apellido">
    <br>
  <label for"">TELEFONO:</label>
    <br>
    <input type="text" class="form-control" value="<?php echo $persona->telefono_per; ?>" type="text" name="telefono_per" id= "telefono_per"value="" placeholder="Ingrese su numero de telefono">
    <br>
  <label for"">EMAIL:</label>
    <br>
    <input type="text" class="form-control" value="<?php echo $persona->email_per; ?>" type="text" name="email_per" id= "email_per"value="" placeholder="Ingrese su email">
    <br>
  <label for"">DIRECCION:</label>
    <br>
    <input type="text" class="form-control" value="<?php echo $persona->direccion_per; ?>" type="text" name="direccion_per" id= "direccion_per"value="" placeholder="Ingrese su direccion">
    <br>
    <br>
    <label for"">ESTADO:</label>
    <select class="form-control" type="text" style="width:60% "name="estado_per" id="estado_per">
    <option value="">--SELECCIONE--</option>
    <option value="ACTIVO">ACTIVO</option>
    <option value="INACTIVO">INACTIVO</option>
  </select>
  <br>
  <br>
  <label for="">FOTOGRAFIA</label>
  <input type="file" name="foto_per"
  accept="image/*"
  id="foto_per" value="">

    <br>
    <button type="submit" name="button"  class="btn btn-primary"> Actualizar</a></button>
    <!--PARA DAR ESPACICOS HACIA LA DERECHA-->
    &nbsp;&nbsp;&nbsp;
  <a href="<?php echo site_url(); ?>/personas/index" class="btn btn-warning">
     Cancelar
   </a>  </div>
     <div class="col-md-12"></div>
 </center>
</form>
<br>
<script type="text/javascript">
// Activado el pais seleccionado para el cliente
$("#fk_id_gen").val("<?php  echo $persona->fk_id_gen; ?>");
$("#estado_per").val("<?php  echo $persona->estado_per; ?>");

</script>
<script type="text/javascript">
    $("#frm_nuevo_persona").validate({
      rules:{
        fk_id_gen:{
          required:true
        },
        identificacion_per:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
        nombre_per:{
          letras:true,
          required:true
          },
          telefono_per:{
          required:true,
          minlength:9,
          maxlength:10,
          digits:true,

          },
          email_per:{
          required:true,
          email:true,
          },
          direccion_per:{
          required:true,
          },
        apellido_per:{
          letras:true,
          required:true
        }
      },
      messages:{
        fk_id_gen:{
          required:"Por favor seleccione el genero"
        },
        identificacion_per:{
          required:"Por favor ingrese el número de cédula",
          minlength:"La cédula debe tener mínimo 10 digitos",
          maxlength:"La cédula debe tener máximo 10 digitos",
          digits:"La cédula solo acepta números"
        },
        nombre_per:{
          letras:"Apellido Incorrecto",
          required:"Por favor ingrese el nombre"
        },
        telefono_per:{
  required:"Por favo ingrese el Telefono",
  minlength:"Telefono debe tener 9 dijitos",
  maxlength:"Telefono debe tener 10 dijitos",
  digits:"Solo acepta numeros"

},
email_per:{
  required:"Solo acepta correos electronicos validos",
  email:"Correro incorrecto"
},
direccion_per:{
  required:"Ingrese la direccion",
},
        apellido_per:{
          letras:"Apellido Incorrecto",
          required:"Por favor ingrese el apellido"
        }
      }
    });
</script>
<script type="text/javascript">
  $("#foto_per").fileinput({
    allowedFileExtension:["jpeg","jpg","png"],
    dropZoneEnabled:true,
    language:"es"
  });
</script>
