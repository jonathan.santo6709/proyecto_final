<br>
<form  action="<?php echo site_url(); ?>/personas/guardarPersona" method="post" id="frm_nuevo_persona"  enctype="multipart/form-data">

<!-- <div class="row"> -->
<table class="table table-success table-striped">
  <div class="col-md-10" >
<div class="row">
  <div class="col-md-4" ></div>
      <div class="col-md-8">
      <center>
        <b><label for="">GENERO</label></b>
        <select class="form-control" name="fk_id_gen" id="fk_id_gen" required >
          <option value="">SELECCIONE UN GENERO PORFAVOR</option>
          <?php if ($listadoGeneros): ?>
          <?php foreach ($listadoGeneros->result() as $generoTemporal): ?>
            <option value="<?php echo $generoTemporal->id_gen; ?>">
              <?php echo $generoTemporal->nombre_gen; ?>
            </option>
          <?php endforeach; ?>
        <?php endif; ?>

        </select>
        <br>
        <br>
          <label for"">IDENTIFICACION:</label>
            <br>
            <input type="number" class="form-control"  type="text" name="identificacion_per" id="identificacion_per" value="" placeholder="Por favor ingrese la identificacion">
            <br>
        <label for"">NOMBRE:</label>
            <br>
            <input type="text" class="form-control"  type="text" name="nombre_per" id= "nombre_per"value="" placeholder="Ingrese su nombre">
            <br>
          <label for"">APELLIDO:</label>
            <br>
            <input type="text" class="form-control"  type="text" name="apellido_per" id= "apellido_per"value="" placeholder="Ingrese el apellido">
            <br>
          <label for"">TELEFONO:</label>
            <br>
            <input type="number" class="form-control"  type="text" name="telefono_per" id= "telefono_per"value="" placeholder="Ingrese su numero de telefono">
            <br>
          <label for"">EMAIL:</label>
            <br>
            <input type="text" class="form-control"  type="text" name="email_per" id= "email_per"value="" placeholder="Ingrese su email">
            <br>
          <label for"">DIRECCION:</label>
            <br>
            <input type="text" class="form-control"  type="text" name="direccion_per" id= "direccion_per"value="" placeholder="Ingrese su direccion">
            <br>
            <br>
            <label for"">ESTADO:</label>
            <select class="form-control" type="text" style="width:60% "name="estado_per" id="estado_per">
            <option value="">--SELECCIONE--</option>
            <option value="ACTIVO">ACTIVO</option>
            <option value="INACTIVO">INACTIVO</option>
          </select>
          <br>
          <br>
          <label for="">FOTOGRAFIA</label>
          <input type="file" name="foto_per"
          accept="image/*"
          id="foto_per" value="">

            <br>

            <button type="submit" name="button"  class="btn btn-primary"> Agregar</a></button>

            <!--PARA DAR ESPACICOS HACIA LA DERECHA-->
            &nbsp;&nbsp;&nbsp;
          <a href="<?php echo site_url(); ?>/personas/index" class="btn btn-warning">
             Cancelar
           </a>
         </div>
             <div class="col-md-12"></div>
         </center>
        </form>
        <br>
        <script type="text/javascript">
        // Activado el pais seleccionado para el cliente
        $("#fk_id_genero").val("<?php  echo $persona->fk_id_gen; ?>");
        $("#estado_per").val("<?php  echo $persona->estado_per; ?>");

        </script>
<script type="text/javascript">
      function validateDni(){
      var numero = $("#identificacion_per").val();
      console.log("numero_",numero);
      var suma = 0;
      var residuo = 0;
      var pri = false;
      var pub = false;
      var nat = false;
      var numeroProvincias = 22;
      var modulo = 11;

      /* Verifico que el campo no contenga letras */
      var ok=1;
      for (i=0; i<numero.length && ok==1 ; i++){
         var n = parseInt(numero.charAt(i));
         if (isNaN(n)) ok=0;
      }
      if (ok==0){
         //alert("No puede ingresar caracteres en el número");
         return false;
      }

      if (numero.length < 10 ){
      //   alert('El número ingresado no es válido');
         return false;
      }

      /* Los primeros dos digitos corresponden al codigo de la provincia */
      provincia = numero.substr(0,2);
      if (provincia < 1 || provincia > numeroProvincias){
      //   alert('El código de la provincia (dos primeros dígitos) es inválido');
     return false;
      }

      /* Aqui almacenamos los digitos de la cedula en variables. */
      d1  = numero.substr(0,1);
      d2  = numero.substr(1,1);
      d3  = numero.substr(2,1);
      d4  = numero.substr(3,1);
      d5  = numero.substr(4,1);
      d6  = numero.substr(5,1);
      d7  = numero.substr(6,1);
      d8  = numero.substr(7,1);
      d9  = numero.substr(8,1);
      d10 = numero.substr(9,1);

      /* El tercer digito es: */
      /* 9 para sociedades privadas y extranjeros   */
      /* 6 para sociedades publicas */
      /* menor que 6 (0,1,2,3,4,5) para personas naturales */

      if (d3==7 || d3==8){
         //alert('El tercer dígito ingresado es inválido');
         return false;
      }

      /* Solo para personas naturales (modulo 10) */
      if (d3 < 6){
         nat = true;
         p1 = d1 * 2;  if (p1 >= 10) p1 -= 9;
         p2 = d2 * 1;  if (p2 >= 10) p2 -= 9;
         p3 = d3 * 2;  if (p3 >= 10) p3 -= 9;
         p4 = d4 * 1;  if (p4 >= 10) p4 -= 9;
         p5 = d5 * 2;  if (p5 >= 10) p5 -= 9;
         p6 = d6 * 1;  if (p6 >= 10) p6 -= 9;
         p7 = d7 * 2;  if (p7 >= 10) p7 -= 9;
         p8 = d8 * 1;  if (p8 >= 10) p8 -= 9;
         p9 = d9 * 2;  if (p9 >= 10) p9 -= 9;
         modulo = 10;
      }

      /* Solo para sociedades publicas (modulo 11) */
      /* Aqui el digito verficador esta en la posicion 9, en las otras 2 en la pos. 10 */
      else if(d3 == 6){
         pub = true;
         p1 = d1 * 3;
         p2 = d2 * 2;
         p3 = d3 * 7;
         p4 = d4 * 6;
         p5 = d5 * 5;
         p6 = d6 * 4;
         p7 = d7 * 3;
         p8 = d8 * 2;
         p9 = 0;
      }

      /* Solo para entidades privadas (modulo 11) */
      else if(d3 == 9) {
         pri = true;
         p1 = d1 * 4;
         p2 = d2 * 3;
         p3 = d3 * 2;
         p4 = d4 * 7;
         p5 = d5 * 6;
         p6 = d6 * 5;
         p7 = d7 * 4;
         p8 = d8 * 3;
         p9 = d9 * 2;
      }

      suma = p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8 + p9;
      residuo = suma % modulo;

      /* Si residuo=0, dig.ver.=0, caso contrario 10 - residuo*/
      digitoVerificador = residuo==0 ? 0: modulo - residuo;

      /* ahora comparamos el elemento de la posicion 10 con el dig. ver.*/
      if (pub==true){
         if (digitoVerificador != d9){
          //  alert('El ruc de la empresa del sector público es incorrecto.');
            return false;
         }
         /* El ruc de las empresas del sector publico terminan con 0001*/
         if ( numero.substr(9,4) != '0001' ){
          //  alert('El ruc de la empresa del sector público debe terminar con 0001');
            return false;
         }
      }
      else if(pri == true){
         if (digitoVerificador != d10){
            document.getElementById("salida").innerHTML = ("El ruc de la empresa del sector privado es incorrecto.");

            return false;
         }
         if ( numero.substr(10,3) != '001' ){
           document.getElementById("salida").innerHTML = ("El ruc de la empresa del sector privado debe terminar con 001");
            return false;
         }
      }

      else if(nat == true){
         if (digitoVerificador != d10){
           document.getElementById("salida").innerHTML = ("El número de cédula de la persona natural es incorrecto");
            return false;
         }
         if (numero.length >10 && numero.substr(10,3) != '001' ){
           document.getElementById("salida").innerHTML = ("El numero de la persona natural debe terminar con 001");
            return false;
         }
      }
      return true;
      }
            $("#frm_nuevo_persona").validate({
              rules:{
                fk_id_gen:{
                  required:true
                },
                identificacion_per:{
                  required:true,
                  minlength:10,
                  maxlength:10,
                  digits:true
                },
                nombre_per:{
                  letras:true,
                  required:true
                  },
                  telefono_per:{
                  required:true,
                  minlength:9,
                  maxlength:10,
                  digits:true,

                },
                email_per:{
                  required:true,
                  email:true,
                },
                direccion_per:{
                  required:true,
                },
                apellido_per:{
                  letras:true,
                  required:true
                }
              },
              messages:{
                fk_id_pais:{
                  required:"Por favor seleccione el genero"
                },
                identificacion_per:{
                  required:"Por favor ingrese el número de cédula",
                  minlength:"La cédula debe tener mínimo 10 digitos",
                  maxlength:"La cédula debe tener máximo 10 digitos",
                  digits:"La cédula solo acepta números"
                },
                nombre_per:{
                  letras:"Apellido Incorrecto",
                  required:"Por favor ingrese el nombre"
                },
                telefono_per:{
          required:"Por favo ingrese el Telefono",
          minlength:"Telefono debe tener 9 dijitos",
          maxlength:"Telefono debe tener 10 dijitos",
          digits:"Solo acepta numeros"

        },
        email_per:{
          required:"Solo acepta correos electronicos validos",
          email:"Correro incorrecto"
        },
        direccion_per:{
          required:"Ingrese la direccion",
        },
                apellido_per:{
                  letras:"Apellido Incorrecto",
                  required:"Por favor ingrese el apellido"
                }
              }
            });
        </script>

        <script type="text/javascript">
          $("#foto_per").fileinput({
            allowedFileExtension:["jpeg","jpg","png"],
            dropZoneEnabled:true,
            language:"es"
          });
        </script>
