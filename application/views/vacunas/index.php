<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<br>
<h1 class="text-center">GESTION DE VACUNAS</h1>
<br>
<center>
  <button type="button" name="button" class="btn btn-primary"
   onclick="cargarListadoVacunas();"><i class="fa fa-refresh" title="Actualizar"></i> </button>
   <!-- Trigger the modal with a button -->
   <button type="button" class="btn btn-info " data-toggle="modal" data-target="#modalNuevoVacuna"><i class="fa fa-plus-circle"></i> Agregar Vacuna</button>
</center>
<div class="row">
  <div class="col-md-12"
  id="contenedor-listado-vacunas">
    <i class="fa fa-spin fa-lg fa-spinner"></i>
    Consultando Datos
  </div>
</div>


 <!-- Modal -->
 <div id="modalNuevoVacuna"
 style="z-index:9999 !important;"
  class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">

     <!-- Modal content-->
     <div class="modal-content">
       <div class="modal-header">
         <h4 class="modal-title"><i class="fa fa-users"></i> Nuevo Vacuna</h4>
         <button type="button" class="close" data-dismiss="modal">&times;</button>
       </div>
       <div class="modal-body">
         <form class="" action="<?php echo site_url('vacunas/insertarVacuna'); ?>" method="post"
         id="frm_nuevo_vacuna">
         <label for="">NOMBRE:</label>
         <br>
         <input type="text" name="nombre_vac"
         id="nombre_vac" class="form-control"> <br>

         <label for="">DESCRIPCIÓN:</label>
         <br>
         <input type="text" name="descripcion_vac"
         id="descripcion_vac" class="form-control"> <br>

         <label for="">COSTO:</label>
         <br>
         <input type="number" name="costo_vac"
         id="costo_vac" class="form-control"> <br>

         <label for="">MILIGRAMOS:</label>
         <br>
         <input type="number" name="miligramos_vac"
         id="miligramos_vac" class="form-control"> <br>

         <label for="">FOTO:</label>
         <br>
         <input type="file" name="miligramos_vac"
         id="foto_vac" class="form-control"> <br>
         <!--
         <label for="">DOSIS:</label>
         <br>
         <input type="text" name="fk_id_ds"
         id="fk_id_ds" class="form-control"> <br>
        -->
         <br>
         <button type="submit" name="button"
         class="btn btn-success">
           <i class="fa fa-save"></i> Guardar
         </button>

         </form>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
       </div>
     </div>

   </div>
 </div>


 <div id="modalEditarVacuna"
 style="z-index:9999 !important;"
  class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">

     <!-- Modal content-->
     <div class="modal-content">
       <div class="modal-header">
         <h4 class="modal-title"><i class="fa fa-users"></i> EDITAR VACUNA</h4>
         <button type="button" class="close" data-dismiss="modal">&times;</button>
       </div>
       <div class="modal-body"id="contenedor-formulario-editar">

       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
       </div>
     </div>

   </div>
 </div>


<script type="text/javascript">
    function cargarListadoVacunas(){
      $("#contenedor-listado-vacunas")
      .html('<i class="fa fa-spin fa-lg fa-spinner"></i>');
      $("#contenedor-listado-vacunas")

      .load("<?php echo site_url(); ?>/vacunas/listado");
    }
    cargarListadoVacunas();
    $("#frm_nuevo_vacuna").validate({
      rules:{
        nombre_vac:{
          required:true,
        },
        descripcion_vac:{
          required:true,
        },
        costo_vac:{
          required:true,
        },
        miligramos_vac:{
          required:true
        },

      },
      messages:{
        nombre_vac:{
          required:"Por favor ingrese el nombre",
        },
        descripcion_vac:{
          required:"Por favor ingrese la descripcion",
        },
        costo_vac:{
          required:"Por favor ingrese el costo",
        },

      },
      submitHandler:function(form){//funcion para peticiones AJAX
          $.ajax({
            url:$(form).prop("action"),
            type:"post",
            data:$(form).serialize(),
            success:function(data){
                cargarListadoVacunas();
                $("#modalNuevoVacuna").modal("hide");
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                var objetoJson=JSON.parse(data);
                  if(objetoJson.respuesta=="ok"||objetoJson.respuesta=="OK"){
                    iziToast.success({
                         title: 'CONFIRMACIÓN',
                         message: 'Vacuna Insertado con exito',
                         position: 'topRight',
                       });
                  }else{
                    iziToast.error({
                         title: 'ERROR',
                         message: 'ERROR',
                         position: 'topRight',
                       });
                  }



                //backdrop
                alert(data);
            }
          });
      }
    });
</script>
<script type="text/javascript">
  function abrirFormularioEditar(id_vac){
    //alert("ok....");
    $("#contenedor-formulario-editar")
    .html('<i class="fa fa-spin fa-lg fa-spinner"></i>');
    $("#contenedor-formulario-editar").load("<?php echo site_url('vacunas/editar'); ?>/"+id_vac);
    $("#modalEditarVacuna").modal("show");
  }

</script>

<!--nueva importacion clase 29/6/2022-->
<script type="text/javascript">

  $('#foto_vac').fileinput({
    allowedFileExtensions:['jpeg','jpg','png'],
    dropZoneEnable:true,
    language:'es'

  });
</script>



<!--  -->
