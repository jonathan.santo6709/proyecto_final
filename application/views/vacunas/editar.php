
<form class=""
action="<?php echo site_url('vacunas/actualizarVacunaAjax'); ?>"
method="post"
id="frm_actualizar_vacuna">

    <input type="hidden" name="id_vac" id="id_vac"
    value="<?php echo $vacuna->id_vac; ?>">

    <label for="">NOMBRE:</label><br>
    <input type="text" name="nombre_vac"
    value="<?php echo $vacuna->nombre_vac; ?>"
    id="nombre_vac_editar" class="form-control" required> <br>

    <label for="">DESCRIPCION:</label><br>
    <input type="text" name="descripcion_vac"
    value="<?php echo $vacuna->descripcion_vac; ?>"
    id="descripcion_vac_editar" class="form-control"> <br>

    <label for="">COSTO:</label><br>
    <input type="text" name="costo_vac"
    value="<?php echo $vacuna->costo_vac; ?>"
    id="costo_vac_editar" class="form-control"> <br>

    <label for="">MILIGRAMOS:</label><br>
    <input type="number" name="miligramos_vac"
    value="<?php echo $vacuna->miligramos_vac; ?>"
    id="miligramos_vac_editar" class="form-control"> <br>

    <!--para crear y poner una foto accept para que unicamente seleccione imagenes-->
    <br>
    <br>
    <b>FOTOGRAFIA: </b>
    <input type="file" class="form-control"
    value="<?php echo $vacuna->foto_vac; ?>"
    name="vacuna_vac" id='foto_vac_editar' class="form-control input-sm "
    accept="image/*"  >
    <br>
    <br>
    <!--hasta aqui antes del boton guardar -->


    <label for="">DOSIS:</label>
    <br>
    <input class="form-control" name="fk_id_ds"
    id="fk_id_ds_editar">
  </input>
    <script type="text/javascript">
        $("#fk_id_ds_editar").val("<?php echo $vacuna->fk_id_ds; ?>");
    </script>
    <br>
    <button type="button" onclick="actualizar();" name="button"
    class="btn btn-success">
      <i class="fa fa-pen"></i> Actualizar
    </button>
</form>




<script type="text/javascript">

function actualizar(){

    $.ajax({
      url:$("#frm_actualizar_vacuna").prop("action"),
      data:$("#frm_actualizar_vacuna").serialize(),
      type:"post",
      success:function(data){
        cargarListadoVacunas();
        $("#modalEditarVacuna").modal("hide");
        $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
        $('.modal-backdrop').remove();//eliminamos el backdrop del modal
        var objetoJson=JSON.parse(data);
        if(objetoJson.respuesta=="ok" || objetoJson.respuesta=="OK"){
          iziToast.success({
               title: 'CONFIRMACIÓN',
               message: 'Actualización Exitosa',
               position: 'topRight',
             });
        }else{
          iziToast.error({
               title: 'ERROR',
               message: 'Error al procesar',
               position: 'topRight',
             });
        }

      }
    });
}

</script>

<script type="text/javascript">

  $('#foto_vac').fileinput({
    allowedFileExtensions:['jpeg','jpg','png'],
    dropZoneEnable:true,
    language:'es'

  });



</script>
