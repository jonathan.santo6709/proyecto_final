<center>
  <h1><b>MINISTERIO DE SALUD PÚBLICA</b></h1>
  <hr>
<br>
<p>La vacunación es una forma sencilla, inocua y eficaz de protegernos contra enfermedades dañinas antes de entrar en contacto con ellas. Las vacunas activan las defensas naturales del organismo para que aprendan a resistir a infecciones específicas, y fortalecen el sistema inmunitario.

Tras vacunarnos, nuestro sistema inmunitario produce anticuerpos, como ocurre cuando nos exponemos a una enfermedad, con la diferencia de que las vacunas contienen solamente microbios (como virus o bacterias) muertos o debilitados y no causan enfermedades ni complicaciones.

La mayoría de las vacunas se inyectan, pero otras se ingieren (vía oral) o se nebulizan en la nariz.</p>
<br>
<br>
<img src="<?php echo base_url();?>/assets/images/vacuna.jpg" width="100%" alt="logo"/>
</center>
