<br>
<form  action="<?php echo site_url(); ?>/dosis/procesarActualizacion" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_ds"  id="id_ds" value="<?php echo $dosi->id_ds; ?>">
<br>
<br>
<!-- <div class="row"> -->
<table class="table table-success table-striped">

    <div class="col-md-10" >
  <div class="row">
    <div class="col-md-4" ></div>
        <div class="col-md-8">
          <center>
            <b><label for="">VACUNA</label></b>
            <select class="form-control" name="fk_id_vac" id="fk_id_vac" required >
              <option value="">SELECCIONE LA VACUNA PORFAVOR</option>
              <?php if ($listadoVacunas): ?>
              <?php foreach ($listadoVacunas->result() as $vacunaTemporal): ?>
                <option value="<?php echo $vacunaTemporal->id_vac; ?>">
                  <?php echo $vacunaTemporal->nombre_vac; ?>
                </option>
              <?php endforeach; ?>
            <?php endif; ?>

            </select>
            <br>

            <b><label for="">PERSONA</label></b>
            <select class="form-control" name="fk_id_per" id="fk_id_per" required >
              <option value="">SELECCIONE LA PERSONA PORFAVOR</option>
              <?php if ($listadoPersonas): ?>
              <?php foreach ($listadoPersonas->result() as $personaTemporal): ?>
                <option value="<?php echo $personaTemporal->id_per; ?>">
                  <?php echo $personaTemporal->nombre_per; ?>
                  <?php echo $personaTemporal->apellido_per; ?>
                </option>
              <?php endforeach; ?>
            <?php endif; ?>

            </select>
            <br>
  <label for"">DOSIS:</label>
    <br>
    <input type="text" class="form-control" value="<?php echo $dosi->numero_ds; ?>" type="text" name="numero_ds" id="numero_ds" value="" placeholder="Por favor ingrese la numero de dosis">
    <br>
<label for"">LUGAR:</label>
    <br>
    <input type="text" class="form-control" value="<?php echo $dosi->lugar_ds; ?>" type="text" name="lugar_ds" id= "lugar_ds"value="" placeholder="Ingrese su lugar de vacunacion">
    <br>
  <label for"">FECHA:</label>
    <br>
    <input type="date" class="form-control" value="<?php echo $dosi->fecha_dosis_ds; ?>" type="text" name="fecha_dosis_ds" id= "fecha_dosis_ds"value="" placeholder="Ingrese la fecha">
    <br>
    <br>
    <button type="submit" name="button"  class="btn btn-primary"> Actualizar</a></button>
    <!--PARA DAR ESPACICOS HACIA LA DERECHA-->
    &nbsp;&nbsp;&nbsp;
  <a href="<?php echo site_url(); ?>/dosis/index" class="btn btn-warning">
     Cancelar
   </a>  </div>
     <div class="col-md-12"></div>
 </center>
</form>
<br>
<script type="text/javascript">
// Activado el pais seleccionado para el cliente
$("#fk_id_vac").val("<?php  echo $dosi->fk_id_vac; ?>");
$("#fk_id_per").val("<?php  echo $dosi->fk_id_per; ?>");

</script>
<script type="text/javascript">
    $("#frm_nuevo_dosi").validate({
      rules:{
        fk_id_vac:{
          required:true
        },
        fk_id_per:{
          required:true
        },
        numero_ds:{
          letras:true,
          required:true
          },
        lugar_ds:{
          letras:true,
          required:true
        },
        fecha_dosis_ds:{
          letras:true,
          required:true
        }
      },
      messages:{
        fk_id_vac:{
          required:"Por favor seleccione la vacuna"
        },
        fk_id_per:{
          required:"Por favor seleccione la persona"
        },
        numero_ds:{
          letras:"nombre incorrecto",
          required:"Por favor ingrese el nombre"
        },
        lugar_ds:{
          letras:"Lugar Incorrecto",
          required:"Por favor ingrese el lugar"
        },
        fecha_dosis_ds:{
          letras:"fecha Incorrecto",
          required:"Por favor ingrese el fecha"
        }
      }
    });
</script>
