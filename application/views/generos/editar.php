<form action="<?php echo site_url(); ?>/generos/procesarActualizacion" method="post" id='frm_nuevo_genero'>
    <div class="col-md-12" >
<div class="row">
    <div class="col-md-4" ></div>
        <div class="col-md-8">
          <br>
          <center>
          <h1> <b>FORMULARIO DE REGISTRO</b>  </h1>
          <br>
          </center>
          <br>
            <center>
            <input type="hidden" name="id_gen" id="id_gen"value="<?php echo $genero->id_gen; ?>">
            <select class="form-control" type="text" name="nombre_gen" id="nombre_gen">
                <option>-----SELECCIONE-----</option>
                <option>MASCULINO</option>
                <option>FEMENINO</option>
                </select><br>

                <button class="btn btn-info " type="submit" name="button">Actualizar</button>
                &nbsp; &nbsp;&nbsp
                <a href="<?php echo site_url(); ?>/generos/index" class="btn btn-warning"><i class="fa fa-times"></i> Cancelar</a>
              </center>
            </div>
        <div class="col-md-2"></div>
        </div>
    </div>
    </form>
    <script type="text/javascript">
        $("#nombre_gen"). val("<?php echo $genero->nombre_gen; ?>");
    </script>

    <script type="text/javascript">
        $("#frm_nuevo_genero").validate({
          rules:{
            nombre_gen:{
              required:true
            }
          },
          messages:{
            nombre_gen:{
              required:"Porfavor seleccione el genero"
            }
          }
        });
    </script>
