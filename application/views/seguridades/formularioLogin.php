<!DOCTYPE html>
<html lang="en">
<head>
	<title>Inicio Sesion</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="<?php echo base_url();?>/assets/assets/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/assets/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/assets/vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/assets/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/assets/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/assets/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/assets/css/main.css">
<!--===============================================================================================-->
</head>
<body>

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="<?php echo base_url();?>/assets/assets/images/msp.jpg" alt="MINISTERIO SALUD PUBLICA">
				</div>

				<form class="login100-form validate-form" action="<?php echo site_url();?>/seguridades/validarAcceso" method= "post">
					<span class="login100-form-title">
						Iniciar Sesion
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input100" type="email" name="email_usu" id="email_usu" placeholder="Email" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required" required>
						<input class="input100" type="password" name="password_usu" id="password_usu"  name="pass" placeholder="Contraseña">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Entrar
						</button>
					</div>

					<div class="text-center p-t-12">
						<a class="txt2" href="#" data-toggle="modal" data-target="#modalRecuperarUsuario">
					    ¿Olvidaste la Contraseña?
						</a>
					</div>

					<div class="text-center p-t-136">
						<a class="txt2" href="<?php echo site_url(); ?>/seguridades/formularioRegistro">
							Registrarse
							<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>

  <!-- Modal -->
 <div id="modalRecuperarUsuario"
 style="z-index:9999 !important;"
  class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">

     <!-- Modal content-->
     <div class="modal-content">
       <div class="modal-header">
         <h4 class="modal-title"><i class="fa fa-users"></i> Recuperar Usuario</h4>
         <button type="button" class="close" data-dismiss="modal">&times;</button>
       </div>
       <div class="modal-body">
       <form class="" action="<?php echo site_url("seguridades/recuperarPassword"); ?>" method="post">
       <div class="form-group">
                  <label for="exampleInputEmail">Ingrese correo de recuperacion:</label>
                  <div class="input-group">
                    <div class="input-group-prepend bg-transparent">
                      <span class="input-group-text bg-transparent border-right-0">
                        <i class="mdi mdi-account-outline text-primary"></i>
                      </span>
                    </div>
                    <br>

                    <input type="email" class="form-control form-control-lg border-left-0" name="email" value="" placeholder="Ingrese su correo electronico" required>
                  </div>
                </div>

              <button type="submit" class="btn btn-primary btn-block mb-3">Enviar Correo</button>

              </button>
              </form>
       </div>
       <div class="modal-footer">
         <button type="submit" class="btn btn-default" data-dismiss="modal">Cerrar</button>
       </div>
     </div>

   </div>
 </div>
 <!-- /. final modal -->

 <!-- Modal -->
  <div id="modalNuevoUsuario"
  style="z-index:9999 !important;"
   class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"><i class="fa fa-users"></i> Nuevo Usuario</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form class=""
          action="<?php echo site_url('usuarios/insertarUsuario'); ?>"
          method="post"
          id="frm_nuevo_usuario">
          <label for="">APELLIDO:</label>
          <br>
          <input type="text" name="apellido_usu"
          id="apellido_usu" class="form-control"> <br>

          <label for="">NOMBRE:</label>
          <br>
          <input type="text" name="nombre_usu"
          id="nombre_usu" class="form-control"> <br>

          <label for="">EMAIL:</label>
          <br>
          <input type="text" name="email_usu"
          id="email_usu" class="form-control"> <br>

          <label for="">CONTRASEÑA:</label>
          <br>
          <input type="password" name="password_usu"
          id="password_usu" class="form-control"> <br>

          <label for="">CONFIRME LA CONTRASEÑA:</label>
          <br>
          <input type="password" name="password_confirmada"
          id="password_confirmada" class="form-control"> <br>

          <label for="">PERFIL:</label>
          <br>
          <select class="form-control" name="perfil_usu"
          id="perfil_usu">
           <option value="">Seleccione una opción</option>
           <option value="ADMINISTRADOR">ADMINISTRADOR</option>
           <option value="VENDEDOR">VENDEDOR</option>
          </select>
          <br>
          <button type="submit" name="button"
          class="btn btn-success">
            <i class="fa fa-save"></i> Guardar
          </button>

          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>

    </div>
  </div>

  <!-- /. modal-final -->
  <?php if ($this->session->flashdata("error")):?>
 <script type="text/javascript">
 alert("<?php echo $this->session->flashdata("error");?>");
 </script>
 <?php endif;?>
   <!-- base:js -->
   <script src="<?php echo base_url();?>assets/vendors/base/vendor.bundle.base.js"></script>
   <!-- endinject -->
   <!-- inject:js -->
   <script src="<?php echo base_url();?>assets/js/off-canvas.js"></script>
   <script src="<?php echo base_url();?>assets/s/hoverable-collapse.js"></script>
   <script src="<?php echo base_url();?>assets/js/template.js"></script>
   <!-- endinject -->
   <script type="text/javascript">
     $("#frm_nuevo_usuario").validate({
       rules:{
         apellido_usu:{
           required:true,
         },
         password_usu:{
           required:true
         },
         password_confirmada:{
           required:true,
           equalTo:"#password_usu"
         }
       },
       messages:{
         apellido_usu:{
           required:"Por favor ingrese el apellido",
         }
       },
       submitHandler:function(form){//funcion para peticiones AJAX
           $.ajax({
             url:$(form).prop("action"),
             type:"post",
             data:$(form).serialize(),
             success:function(data){
                 $("#modalNuevoUsuario").modal("hide");
                 var objetoJson=JSON.parse(data);
                   if(objetoJson.respuesta=="ok"||objetoJson.respuesta=="OK"){
                     iziToast.success({
                          title: 'CONFIRMACIÓN',
                          message: 'Usuario Insertado con exito',
                          position: 'topRight',
                        });
                   }else{
                     iziToast.error({
                          title: 'ERROR',
                          message: 'ERROR',
                          position: 'topRight',
                        });
                   }
                 //backdrop
                 alert(data);
             }
           });
       }
     });
 </script>

<!--===============================================================================================-->
	<script src="<?php echo base_url();?>/assets/assets/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>/assets/assets/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url();?>/assets/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>/assets/assets/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>/assets/assets/vendor/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>
